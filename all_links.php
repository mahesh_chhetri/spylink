<?php
	session_start();
	include 'config.php';
	$link_status=$_GET['s'];
	$links=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM links"));
	$logged_user=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM users WHERE user_id='".$_SESSION['user_id']."'"));	
	if(isset($_POST['block'])){
		mysqli_query($conn,"UPDATE links SET status='blocked' WHERE id='".$_POST['id']."'");
		$alert='<div class="alert alert-success"><i class="fa fa-lock"></i>&nbsp;&nbsp;Link Blocked</div>';
	}
	
	if(isset($_POST['unblock'])){
		mysqli_query($conn,"UPDATE links SET status='active' WHERE id='".$_POST['id']."'");
		$alert='<div class="alert alert-success"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Link Actived</div>';
	}
	
	if(isset($_POST['remove'])){
		mysqli_query($conn,"UPDATE links SET status='removed' WHERE id='".$_POST['id']."'");
		$alert='<div class="alert alert-success"><i class="fa fa-remove"></i>&nbsp;&nbsp;Link Removed</div>';
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Edit Links</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- select2 CSS -->
    <link href="vendors/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- Daterangepicker CSS -->
    <link href="vendors/daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
    <script src='dist/jquery-2.1.3.min.js'></script>
    <script src="dist/jquery.min.js"></script>
    <!-- Toggles CSS -->
    <link href="vendors/jquery-toggles/css/toggles.css" rel="stylesheet" type="text/css">
    <link href="vendors/jquery-toggles/css/themes/toggles-light.css" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <link href="themeV4/css/common.css" rel="stylesheet" type="text/css">
</head>


<body>
    <!-- Preloader -->
    <div class="preloader-it">
        <div class="loader-pendulums"></div>
    </div>
    <!-- /Preloader -->

    <div id="wrapper">
        <!-- Including sidebar -->
        <?php include 'themeV4/partials/sidebar.php'?>
        <!-- Sidebar Toggle element -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>
        <!-- Main Content -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include 'themeV4/partials/header.php'?>
                <!-- End of Topbar -->
                <!-- Scroll to Top Button-->
                <a class="scroll-to-top rounded" href="#page-top" style="display: none;">
                    <i class="fas fa-angle-up"></i>
                </a>
                <!-- Logout Modal-->
                <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">Select "Logout" below if you are ready to end your current session.
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

                                <form action="logout.php" method="POST">

                                    <button type="submit" name="logout_btn" class="btn btn-primary">Logout</button>

                                </form>


                            </div>
                        </div>
                    </div>
                </div>

                <!-- meta tags-->
                <!-- <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
                <link rel="stylesheet" href="assets/fonts/css/font-awesome.min.css">
                <link rel="icon" href="favicon.png" sizes="16x16" type="image/png">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script src="assets/js/popper.min.js"></script>
                <script src="assets/js/jquery-1.9.1.min.js"></script>
                <script src="assets/js/bootstrap.min.js"></script>
                <script src="assets/js/bootstrap.bundle.min.js"></script> -->


                 <!-- Breadcrumb -->
                 <nav class="hk-breadcrumb" aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-light bg-transparent">
                            <li class="breadcrumb-item"><a href="#">Settings</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Links</li>
                        </ol>
                    </nav>
                <div id="result"></div>

                    <!-- Title -->
                    <?php echo $alert;?>
                    <div class="row">
                    <div class="col-xl-12">
                        <section class="hk-sec-wrapper">
                            <div class="table-wrap">
                                <table id="datable_1" class="table w-100 display pb-30">
                                    <thead>
                                        <tr>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">
                                                #</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Link
                                            </th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">
                                                Created</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Views
                                            </th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Status
                                            </th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Action
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
							 $query   = "select * from links where user_id='" . $logged_user['user_id'] . "' and status='".$link_status."' ORDER BY id DESC";
								$results = mysqli_query($conn, $query);
								
								$s=0;
								while($row=mysqli_fetch_assoc($results)) {
								if ($row['status'] == 'active') {
									$transtatus = 'success';
								} else {
									$transtatus = 'danger';
								}
							$views=mysqli_num_rows(mysqli_query($conn,"SELECT * FROM views WHERE link_id='".$row['link_id']."'"));
							$s++;
								echo '
								 <td class="table_cell">'.$s.'</td>
								 <td class="table_cell">http://'.$_SERVER['HTTP_HOST'].'/gets/'.$row['get_id'].'</td>
								 <td class="table_cell">'.date("d M Y",strtotime($row['date'])).'</td>
								 <td class="table_cell">'.number_format($views).'</td>
								 <td class="table_cell"><span class="badge badge-' . $transtatus . '">' . strtoupper($row['status']) . '</span></td>
								 <td class="table_cell">
									<form action="" method="post">
										<input type="hidden" name="id" value="'.$row['id'].'"/>
										
										<div class="btn-group">
                                            <button type="button" class="btn btn-light">Action</button>
                                            <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div class="dropdown-menu">
                                              <a class="dropdown-item" href="edit.php?id='.$row['link_id'].'"><i class="fa fa-pencil"></i> Edit</a>
                                                <a class="dropdown-item" href="views/'.$row['link_id'].'"><i class="fa fa-eye"></i> View Link</a>
										<button name="block" class="dropdown-item"><i class="fa fa-lock"></i> Block</button>
										<button name="unblock" class="dropdown-item"><i class="fa fa-unlock"></i> Active</button>
										<button name="remove" class="dropdown-item"><i class="fa fa-remove"></i> Remove</button>                                           
										   </div>
                                        </div>
									</form>
									
								 </td>
							  </tr>';
							}
						?>
                                    </tbody>
                                </table>
                            </div>
                        </section>

                    </div>

                <!-- /Title -->
                </div>
                <!-- Bootstrap core JavaScript-->
                <!-- <script src="vendor/jquery/jquery.min.js"></script> -->
                <!-- <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->

                <!-- Core plugin JavaScript-->
                <!-- <script src="vendor/jquery-easing/jquery.easing.min.js"></script> -->

                <!-- Custom scripts for all pages-->
                <!-- <script src="js/sb-admin-2.min.js"></script> -->

                <!-- Page level plugins -->
                <!-- <script src="vendor/chart.js/Chart.min.js"></script> -->

                <!-- Page level custom scripts -->
                <!-- <script src="js/demo/chart-area-demo.js"></script> -->
                <!-- <script src="js/demo/chart-pie-demo.js"></script> -->




                <!-- Footer -->
                <?php include 'themeV4/partials/footer.php'?>
                <!-- End of Footer -->
            </div>
            <!-- End of Main Content -->
        </div>
        <!-- END main  content -->
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Jasny-bootstrap  JavaScript -->
    <script src="vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>

    <!-- Slimscroll JavaScript -->
    <script src="dist/js/jquery.slimscroll.js"></script>

    <!-- Fancy Dropdown JS -->
    <script src="dist/js/dropdown-bootstrap-extended.js"></script>

    <!-- Ion JavaScript -->
    <script src="vendors/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
    <script src="dist/js/rangeslider-data.js"></script>

    <!-- Select2 JavaScript -->
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="vendors/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="dist/js/dataTables-data.js"></script>


    <script src="vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="dist/js/select2-data.js"></script>

    <!-- Bootstrap Tagsinput JavaScript -->
    <!-- <script src="vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script> -->

    <!-- Daterangepicker JavaScript -->
    <script src="vendors/moment/min/moment.min.js"></script>
    <script src="vendors/daterangepicker/daterangepicker.js"></script>
    <script src="dist/js/daterangepicker-data.js"></script>

    <!-- FeatherIcons JavaScript -->
    <script src="dist/js/feather.min.js"></script>

    <!-- Toggles JavaScript -->
    <script src="vendors/jquery-toggles/toggles.min.js"></script>
    <script src="dist/js/toggle-data.js"></script>

    <!-- Init JavaScript -->
    <script src="dist/js/init.js"></script>
</body>

</html>