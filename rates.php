<?php
session_start();
include 'config.php';
$settings=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM settings WHERE id=1"));
?>

<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="full/img/favicon.png" type="image/png">
	<title><?=$settings['site_name']?></title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="themeV4/landingPage/css/bootstrap.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/linericon/style.css">
	<link rel="stylesheet" href="themeV4/landingPage/css/font-awesome.min.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/owl-carousel/owl.carousel.min.css">
	<link rel="stylesheet" href="themeV4/landingPage/css/magnific-popup.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/nice-select/css/nice-select.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/animate-css/animate.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/flaticon/flaticon.css">
	<!-- main css -->
	<link rel="stylesheet" href="themeV4/landingPage/css/style.css">
</head>

<body>
	<!--================Header Menu Area =================-->
	<?php include 'themeV4/partials/header-landing.php'?>
	<!--================Header Menu Area =================-->

	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 70%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 15px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

<center>
<table>
  <tbody><tr>
    <th>COUNTRY</th>
    <th>CPM</th>
    <th>DEVICE</th>
  </tr>
  <tr>
    <td>INDIA</td>
    <td>0.05</td>
    <td>MOBILE/DESKTOP</td>
  </tr>
  <tr>
    <td>ALL COUNTRY</td>
    <td>0.05</td>
    <td>MOBILE/DESKTOP</td>
  </tr>
  
</tbody></table>
</center>


	<!--================Footer Area =================-->
	<?php include 'themeV4/partials/footer-landing.php'?>
	<!--================End Footer Area =================-->

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="themeV4/landingPage/js/jquery-3.2.1.min.js"></script>
	<script src="themeV4/landingPage/js/popper.js"></script>
	<script src="themeV4/landingPage/js/bootstrap.min.js"></script>
	<script src="themeV4/landingPage/js/stellar.js"></script>
	<script src="themeV4/landingPage/js/jquery.magnific-popup.min.js"></script>
	<script src="themeV4/landingPage/vendors/nice-select/js/jquery.nice-select.min.js"></script>
	<script src="themeV4/landingPage/vendors/isotope/imagesloaded.pkgd.min.js"></script>
	<script src="themeV4/landingPage/vendors/isotope/isotope-min.js"></script>
	<script src="themeV4/landingPage/vendors/owl-carousel/owl.carousel.min.js"></script>
	<script src="themeV4/landingPage/js/jquery.ajaxchimp.min.js"></script>
	<script src="themeV4/landingPage/vendors/counter-up/jquery.waypoints.min.js"></script>
	<script src="themeV4/landingPage/vendors/counter-up/jquery.counterup.min.js"></script>
	<script src="themeV4/landingPage/js/mail-script.js"></script>
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="themeV4/landingPage/js/gmaps.min.js"></script>
	<script src="themeV4/landingPage/js/theme.js"></script>
</body>

</html>