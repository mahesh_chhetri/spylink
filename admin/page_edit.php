<?php
error_reporting(0);
session_start();
$alert=NULL;

$alert=NULL;
if (isset($_SESSION['admin'])) {
    include '../config.php';
    $id = $_SESSION['admin'];
} else {
    echo "<script type='text/javascript'>
     window.location.href='index.php';
     </script>";
}

if(isset($_GET['del'])){
	$dd=mysqli_query($conn,"DELETE FROM pages WHERE id='".$_GET['del']."'");
	if($dd){
			header('location:pages.php');
		} else {
			$alert='<div class="alert alert-danger">Failed to Delete</div><meta http-equiv="refresh" content="2">';
		}
}

if(isset($_POST['update'])){
	$nm=mysqli_real_escape_string($conn,$_POST['page_name']);
	$dt=mysqli_real_escape_string($conn,$_POST['page_data']);
	$m=mysqli_query($conn,"UPDATE pages SET page_name='".$nm."', page_data='".$dt."', page_st='".$_POST['page_st']."' WHERE id='".$_GET['id']."'");
	if($m){
		$alert='<div class="alert alert-success">Updated</div>';
	} else {
		$alert='<div class="alert alert-danger">Failed, '.mysqli_error($conn).'</div>';
	}
}

$links=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM pages WHERE id='".$_GET['id']."'"));

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo $links['page_name'];?> - Page Edit</title>
    <meta name="description" content="Neat">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="../assets/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/neat.min.css?v=1.0">
	<script src="assets/js/neat.min.js?v=1.0"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery-1.9.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon">

  </head>
  <body>

    <div class="o-page">
    <?php include 'sidebar.php';?>
      <main class="o-page__content">
        <?php include 'header.php';?>

        <div class="container">
		<?php echo $alert;?>
          <h3>Edit Link <a href="page_edit.php?del=<?php echo $links['id'];?>" onclick="" class="btn btn-danger btn-sm">DELETE</a></h3>
		  <br>
		  <div class="c-card">
		  <form action="" method="post">
		  	<label>Page Name</label>
		   <input  name="page_name" class="form-control" placeholder="Page Title" value="<?php echo $links['page_name'];?>">
			<br>
			<label>Page Content (Fully HTML Support)</label>
			<textarea style="height:200px;" name="page_data" class="form-control" placeholder="Page Content"><?php echo $links['page_data'];?></textarea>
			<br>
				<label>Show This Page in Menu?</label>
		   <select name="page_st" class="form-control">
				<option value="on" <?php if($links['page_st']=='on'){echo'selected';}?>>Show</option>
				<option value="off" <?php if($links['page_st']=='off'){echo'selected';}?>>Hide</option>
		   </select>
			<br>
				<br>
				<input name="update" type="submit" class="c-btn c-btn--success" value="UPDATE"/>
				
				</form>
		  </div>
		<br>
			
        </div>
      </main>
    </div>
<br>
<br>
<br>
    <!-- Main JavaScript -->
    <script src="js/neat.min.js?v=1.0"></script>
	
  </body>
</html>