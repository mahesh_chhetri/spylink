<?php
session_start();
$alert=NULL;

$alert=NULL;
if (isset($_SESSION['admin'])) {
    include '../config.php';
    $id = $_SESSION['admin'];
} else {
    echo "<script type='text/javascript'>
     window.location.href='index.php';
     </script>";
}

    $result    = mysqli_query($conn, "SELECT * FROM users");
    $ttl_users = mysqli_num_rows($result);
	
	$result    = mysqli_query($conn, "SELECT * FROM users where date='".date("Y-m-d")."'");
    $td_ttl_refer = mysqli_num_rows($result);
    
	$result    = mysqli_query($conn, "SELECT * FROM links");
    $ttl_click = mysqli_num_rows($result);
	
	$result   = mysqli_query($conn, "SELECT * FROM transactions where status='paid'");
    $ttl_paid = mysqli_num_rows($result);

	$result  = mysqli_query($conn, "SELECT * FROM links where date='" . date('Y-m-d', strtotime("-6 days")) . "'");
    $sixweek = mysqli_num_rows($result);
    
    $result   = mysqli_query($conn, "SELECT * FROM links where date='" . date('Y-m-d', strtotime("-5 days")) . "'");
    $fiveweek = mysqli_num_rows($result);
    
    $result   = mysqli_query($conn, "SELECT * FROM links where date='" . date('Y-m-d', strtotime("-4 days")) . "'");
    $fourweek = mysqli_num_rows($result);
    
    $result    = mysqli_query($conn, "SELECT * FROM links where date='" . date('Y-m-d', strtotime("-3 days")) . "'");
    $threeweek = mysqli_num_rows($result);
    
    $result  = mysqli_query($conn, "SELECT * FROM links where date='" . date('Y-m-d', strtotime("-2 days")) . "'");
    $twoweek = mysqli_num_rows($result);
    
    $result  = mysqli_query($conn, "SELECT * FROM links where date='" . date('Y-m-d', strtotime("-1 days")) . "'");
    $oneweek = mysqli_num_rows($result);
    
    $result = mysqli_query($conn, "SELECT * FROM links where date='" . date("Y/m/d") . "'");
    $week   = mysqli_num_rows($result);
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dashboard</title>
    <meta name="description" content="Neat">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="../assets/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/neat.min.css?v=1.0">
	<script src="assets/js/neat.min.js?v=1.0"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery-1.9.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon">
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Week', 'Links'],
          ['<?php
				echo date('Y-m-d', strtotime("-6 days"));
			?>',  <?php
				echo $sixweek;
			?>],
					['<?php
				echo date('Y-m-d', strtotime("-5 days"));
			?>',  <?php
				echo $fiveweek;
			?>],
					['<?php
				echo date('Y-m-d', strtotime("-4 days"));
			?>',  <?php
				echo $fourweek;
			?>],
					['<?php
				echo date('Y-m-d', strtotime("-3 days"));
			?>',  <?php
				echo $threeweek;
			?>],
					['<?php
				echo date('Y-m-d', strtotime("-2 days"));
			?>',  <?php
				echo $twoweek;
			?>],
					['<?php
				echo date('Y-m-d', strtotime("-1 days"));
			?>',  <?php
				echo $oneweek;
			?>],
					['Today',  <?php
				echo $week;
			?>]
        ]);

         var options = {
          chart: {
            title: 'Total Generated Links',
            //subtitle: 'Sales, Expenses, and Profit: 2014-2017',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('chart_div'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
    <!---->
  </head>
  <body>

    <div class="o-page">
    <?php include 'sidebar.php';?>
      <main class="o-page__content">
        <?php include 'header.php';?>

        <div class="container">
          <div class="row">
            <div class="col-md-6 col-xl-3">
              <div class="c-card">
                <span class="c-icon c-icon--info u-mb-small">
                  <i class="fa fa-user-o"></i>
                </span>

                <h3 class="c-text--subtitle">Today's Users</h3>
                <h1><?php echo $td_ttl_refer;?></h1>
              </div>
            </div>

            <div class="col-md-6 col-xl-3">
              <div class="c-card">
                <span class="c-icon c-icon--danger u-mb-small">
                  <i class="fa fa-user"></i>
                </span>

                <h3 class="c-text--subtitle">Total Users</h3>
                <h1><?php echo $ttl_users;?></h1>
              </div>
            </div>

            <div class="col-md-6 col-xl-3">
              <div class="c-card">
                <span class="c-icon c-icon--success u-mb-small">
                  <i class="fa fa-link"></i>
                </span>

                <h3 class="c-text--subtitle">Total Clicks</h3>
                <h1><?php echo $ttl_click;?></h1>
              </div>
            </div>

            <div class="col-md-6 col-xl-3">
              <div class="c-card">
                <span class="c-icon c-icon--warning u-mb-small">
                  <i class="fa fa-dollar"></i>
                </span>

                <h3 class="c-text--subtitle">Total Paid Time</h3>
                <h1><?php echo $ttl_paid;?></h1>
              </div>
            </div>
          </div>
   <div class="row">
            <div class="col-md-12">
              <div class="c-card u-ph-zero u-pb-zero">

                <div class="u-ph-medium">
                  <h4>Weekend Generated Links</h4>
                </div>

                <div class="u-p-medium">
                  <div class="c-chart">
                   <div id="chart_div" style="width: 100%; height: 500px;"></div>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
		  
      <h3>Last 10 Signup <a class="c-btn c-btn--success c-btn--small" href="user.php">View All</a></h3>
	  <br>
          <div class="row">
            <div class="col-12">
              <div class="c-table-responsive@wide">
                <table class="c-table">
                  <thead class="c-table__head">
                    <tr class="c-table__row">
                      <th class="c-table__cell c-table__cell--head">Customer</th>
                      <th class="c-table__cell c-table__cell--head">On</th>
                      <th class="c-table__cell c-table__cell--head">User Status</th>
                      <th class="c-table__cell c-table__cell--head">Wallet</th>
                    </tr>
                  </thead>

                  <tbody>
				      <?php
							$query   = "select * from users ORDER BY user_id DESC LIMIT 10";
							$results = mysqli_query($conn, $query);
							while ($row = mysqli_fetch_array($results)) {
								echo '
							 <tr class="c-table__row">
								  <td class="c-table__cell">@'.$row['username'].'</td>
								 <td class="c-table__cell">'.date("d M Y",strtotime($row['date'])).'</td>
								 <td class="c-table__cell"> 
								 <a class="c-badge c-badge--small c-badge--success" href=#">'.$row['status'].'</a>
								 </td>
								  <td class="c-table__cell">'.$currency.''.$row['wallet'].'</td>
							  </tr>';
							}
						?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

			<br>
			<br>
        </div>
      </main>
    </div>
  </body>
</html>