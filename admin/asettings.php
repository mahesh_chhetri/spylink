<?php
session_start();
$alert=NULL;

$alert=NULL;
if (isset($_SESSION['admin'])) {
    include '../config.php';
    $id = $_SESSION['admin'];
} else {
    echo "<script type='text/javascript'>
     window.location.href='index.php';
     </script>";
}



if(isset($_POST['update'])){
	$done=mysqli_query($conn,"UPDATE settings SET captcha='".$_POST['captcha']."' WHERE id=1");
	if($done){
		$alert='<div class="alert alert-success">Settings Updated</div>';
} else {
	$alert='<div class="alert alert-danger">Settings Updated Failed</div>';
}
}

$myset=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM settings WHERE id=1"));
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Others</title>
    <meta name="description" content="Neat">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="../assets/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/neat.min.css?v=1.0">
	<script src="assets/js/neat.min.js?v=1.0"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery-1.9.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon">
  </head>
  <body>

    <div class="o-page">
    <?php include 'sidebar.php';?>
      <main class="o-page__content">
        <?php include 'header.php';?>

        <div class="container p-2">
		<?php echo $alert;?>
          <h3>Other Settings</h3>
		  <br>
<form action="" method="post">

	
	<div class="form-group">
		<label>Captcha in Signup Page</label>
		<select class="form-control" name="captcha">
			<option value="1" <?php if($myset['confirm_account']=='1'){echo'selected';}else{echo'';}?>>ON</option>
			<option value="0" <?php if($myset['confirm_account']=='0'){echo'selected';}else{echo'';}?>>OFF</option>
		</select>
	</div>
	<br>
    <input type="submit" class="c-btn c-btn--warning" name="update" value="Update">
     </form>
		 
        </div>
      </main>
    </div>

    <!-- Main JavaScript -->
    <script src="js/neat.min.js?v=1.0"></script>
  </body>
</html>