<?php
session_start();
$alert=NULL;

$alert=NULL;
if (isset($_SESSION['admin'])) {
    include '../config.php';
    $id = $_SESSION['admin'];
} else {
    echo "<script type='text/javascript'>
     window.location.href='index.php';
     </script>";
}

$settings=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM settings WHERE id='1'"));

if(isset($_POST['update'])){
	$check=mysqli_query($conn,"UPDATE settings SET ref_user='".$_POST['ref_user']."',reg_user='".$_POST['reg_user']."'  WHERE id='1'");
	if($check){
		$alert='<div class="alert alert-success">Refer Setting Updated</div><meta http-equiv="refresh" content="2">';
	} else {
		$alert='<div class="alert alert-danger">Something is wrong</div><meta http-equiv="refresh" content="2">';
	}
}


?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Referral Settings</title>
    <meta name="description" content="Neat">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="../assets/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/neat.min.css?v=1.0">
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script src="assets/js/neat.min.js?v=1.0"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery-1.9.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon">
  </head>
  <body>

    <div class="o-page">
    <?php include 'sidebar.php';?>
      <main class="o-page__content">
        <?php include 'header.php';?>

        <div class="container">
		<?php echo $alert;?>
          <h3>Referral Settings</h3>
		  <br>
		  <div class="c-card">
				<div style="width:100%;" class="card p-4">
					<p>Earning Settings</p>
					<br>
					<form action="" method="post">
						<label>Referring User Can Earn (per refer)</label>
						<br>
						<input class="form-control" name="ref_user" value="<?php echo $settings['ref_user'];?>">
						<br>
						<label>Registring User Can Earn (via refer link only)</label>
						<br>
						<input class="form-control" name="reg_user" value="<?php echo $settings['reg_user'];?>">
						<br>
						<input type="submit" name="update" class="c-btn c-btn--warning" value="UPDATE"/>
					</form>
				</div>
		  </div>
		  <br>
        </div>
      </main>
    </div>
    <!-- Main JavaScript -->
    <script src="js/neat.min.js?v=1.0"></script>
  </body>
</html>