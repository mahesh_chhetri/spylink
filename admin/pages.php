<?php
session_start();
$alert=NULL;

$alert=NULL;
if (isset($_SESSION['admin'])) {
    include '../config.php';
    $id = $_SESSION['admin'];
} else {
    echo "<script type='text/javascript'>
     window.location.href='index.php';
     </script>";
}

if(isset($_POST['dlt'])){
	$dd=mysqli_query($conn,"DELETE FROM pages WHERE id='".$_POST['id']."'");
	if($dd){
			$alert='<div class="alert alert-success">Deleted</div><meta http-equiv="refresh" content="2">';
		} else {
			$alert='<div class="alert alert-danger">Failed to Delete</div><meta http-equiv="refresh" content="2">';
		}
}
	
if(isset($_POST['add'])){
	$nm=mysqli_real_escape_string($conn,$_POST['name']);
		$my=mysqli_query($conn,"INSERT into pages (page_name,page_st) VALUES ('".$nm."','on')");
		if($my){
			$alert='<div class="alert alert-success">New Page Added</div><meta http-equiv="refresh" content="2">';
		} else {
			$alert='<div class="alert alert-danger">Failed to Add</div><meta http-equiv="refresh" content="2">';
		}
	
}
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Add Pages</title>
    <meta name="description" content="Neat">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="../assets/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/neat.min.css?v=1.0">
	<script src="assets/js/neat.min.js?v=1.0"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery-1.9.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon">
  </head>
  <body>

    <div class="o-page">
    <?php include 'sidebar.php';?>
      <main class="o-page__content">
        <?php include 'header.php';?>

        <div class="container">
		<?php echo $alert;?>
          <h3>All Pages</h3>
		  <br>
		  <div class="c-card">
			  <button type="button"  data-toggle="modal" data-target="#myModal" class="c-btn c-btn--success">ADD NEW PAGE</button>
		  </div>
		  <div class="c-table-responsive@wide">
                <table class="c-table">
                  <thead class="c-table__head">
                    <tr class="c-table__row">
                      <th class="c-table__cell c-table__cell--head">S.no</th>
                      <th class="c-table__cell c-table__cell--head">Name</th>
                      <th class="c-table__cell c-table__cell--head">Action</th>
                    </tr>
                  </thead>

                  <tbody>
				      <?php
							$query   = "select * from pages";
							$results = mysqli_query($conn, $query);
							$sn=1;
							while ($row = mysqli_fetch_array($results)) {
								echo '
							 <tr class="c-table__row">
								  <td class="c-table__cell">'.$sn.'</td>
								  <td class="c-table__cell">'.$row['page_name'].'</td>
								  <td style="width:300px;" class="c-table__cell">
								  <a class="btn btn-primary" href="page_edit.php?id='.$row['id'].'">Edit</a>
								  </td>
							  </tr>';
							  $sn++;
							}
						?>
                  </tbody>
                </table>
              </div>
        </div>
      </main>
	    <!-- The Modal -->
    </div>
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add New Page</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
		<form action="" method="post" enctype="multipart/form-data">
		 <label>Page Name</label>
		 <input type="text" name="name" class="c-input">
		 <br>
		<p><b class="text-danger">Note: You can edit page after create</b></p>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <input name="add" class="c-btn c-btn--success" type="submit" value="ADD">
        </div>
         </form>
      </div>
    </div>
  </div>
    <!-- Main JavaScript -->
    <script src="js/neat.min.js?v=1.0"></script>
  </body>
</html>