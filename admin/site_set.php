<?php
session_start();
$alert=NULL;

$alert=NULL;
if (isset($_SESSION['admin'])) {
    include '../config.php';
    $id = $_SESSION['admin'];
} else {
    echo "<script type='text/javascript'>
     window.location.href='index.php';
     </script>";
}

if(isset($_POST['update_logo'])) {
		$filename="logo-light.png";
		$file_tmp=$_FILES['logo']['tmp_name'];
		$folder="../dist/img/";
		if(move_uploaded_file($file_tmp,$folder.$filename)){
			$alert='<div class="alert alert-sucess">Logo Updated</div>';
		} else {
			$alert='<div class="alert alert-danger">Something is wrong, check the file extension (only png allow)</div>';
		}
}

if (isset($_POST['settings_update'])) {
    if ($_POST['settings_update'] =='') {
        $alert='<div class="alert alert-dannger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Failed!</strong> Field blank.
  </div><meta http-equiv="refresh" content="2">';
    } else {
        mysqli_query($conn, "UPDATE settings SET site_name='" . $_POST['site_name'] . "', symbol='" . $_POST['symbol'] . "', payout_limit='" . $_POST['payout_limit'] . "', earn_limit='" . $_POST['earn_limit'] . "', skip_timer='" . $_POST['skip_timer'] . "' WHERE id='1'");
        $alert='<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Update Successful!</strong> Settings has been update.
  </div><meta http-equiv="refresh" content="2">';
    }
}

$settings=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM settings WHERE id='1'"));
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Site Settings</title>
    <meta name="description" content="Neat">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="../assets/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/neat.min.css?v=1.0">
	<script src="assets/js/neat.min.js?v=1.0"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery-1.9.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon">
  </head>
  <body>

    <div class="o-page">
    <?php include 'sidebar.php';?>
      <main class="o-page__content">
        <?php include 'header.php';?>

        <div class="container">
		<?php echo $alert;?>
          <h3>Site Settings</h3>
		  <br>
		  <div class="c-card">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myLogo">Change Logo</button>
		  <br>
		  <br>
		  <hr>
		  <br>
			<form action="" method="post">
     <label>Site Name</label>
     	  <br>
           <input  type="text" class="form-control input-lg" name="site_name" value="<?php
     echo $settings['site_name'];
     ?>"><br>
         <label>Currency Symbol</label>
     	  <br>
           <input  type="text" class="form-control input-lg" name="symbol" value="<?php
     echo $settings['symbol'];
     ?>"><br>
         <label>User Money Withdraw Limit</label>
     	  <br>
           <input  type="text" class="form-control input-lg" name="payout_limit" value="<?php
     echo $settings['payout_limit'];
     ?>"><br>
           <label>Earning Limit (per 1000 View)</label>
     	  <br>
           <input type="text" class="form-control input-lg" name="earn_limit" value="<?php
     echo $settings['earn_limit'];
     ?>">
    <br>
	 <label>Skip Page Timer (in Sec.)</label>
     	  <br>
           <input type="text" class="form-control input-lg" name="skip_timer" value="<?php
     echo $settings['skip_timer'];
     ?>">
    <br>
    <input type="submit" class="c-btn c-btn--warning" name="settings_update" value="Update">
     </form>
		  </div>
        </div>
      </main>
    </div>
	<!-- The Modal -->
<div class="modal fade" id="myLogo">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Change Logo</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
	<form action="" method="post" enctype="multipart/form-data">
      <!-- Modal body -->
      <div class="modal-body">
		<center><img style="height:120px;" id="logo" src="../dist/img/logo-light.png"/></center>
		<br>
		
		<input name="logo" type="file" onchange="document.getElementById('logo').src = window.URL.createObjectURL(this.files[0])" class="form-control">
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button name="update_logo" class="btn btn-success">UPDATE LOGO</button>
      </div>
	  </form>

    </div>
  </div>
</div>
  </body>
</html>