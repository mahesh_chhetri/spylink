<div class="o-page__sidebar js-page-sidebar">
        <aside class="c-sidebar">
          <div class="c-sidebar__brand">
        Admin Panel
          </div>

          <!-- Scrollable -->
          <div class="c-sidebar__body">
            <ul class="c-sidebar__list">
              <li>
                <a class="c-sidebar__link <?php if(basename($_SERVER['PHP_SELF'])=='dash.php'){echo'is-active';}?>" href="dash.php">
                  <i class="c-sidebar__icon fa fa-laptop" aria-hidden="true"></i>Dashboard
                </a>
              </li>
			 <li>
                <a class="c-sidebar__link <?php if(basename($_SERVER['PHP_SELF'])=='site_set.php'){echo'is-active';}?>" href="site_set.php">
                  <i class="c-sidebar__icon fa fa-magic" aria-hidden="true"></i>Site Settings
                </a>
              </li>
			  <li>
                <a class="c-sidebar__link <?php if(basename($_SERVER['PHP_SELF'])=='pages.php'){echo'is-active';}?>" href="pages.php">
                  <i class="c-sidebar__icon fa fa-magic" aria-hidden="true"></i>Site Pages
                </a>
              </li>
			   <li>
                <a class="c-sidebar__link <?php if(basename($_SERVER['PHP_SELF'])=='users.php'){echo'is-active';}?>" href="users.php">
                  <i class="c-sidebar__icon fa fa-user" aria-hidden="true"></i>All Users
                </a>
              </li>
			   <li>
                <a class="c-sidebar__link <?php if(basename($_SERVER['PHP_SELF'])=='transactions.php'){echo'is-active';}?>" href="transactions.php">
                  <i class="c-sidebar__icon fa fa-random" aria-hidden="true"></i>Withdraw Requests
                </a>
              </li>
			  <li>
                <a class="c-sidebar__link <?php if(basename($_SERVER['PHP_SELF'])=='links.php' || basename($_SERVER['PHP_SELF'])=='edit_links.php'){echo'is-active';}?>" href="links.php">
                  <i class="c-sidebar__icon fa fa-link" aria-hidden="true"></i>Manage Links
                </a>
              </li>
			    <li>
                <a class="c-sidebar__link <?php if(basename($_SERVER['PHP_SELF'])=='payments.php'){echo'is-active';}?>" href="payments.php">
                   <i class="c-sidebar__icon fa fa-money" aria-hidden="true"></i>Add Payments Method
                </a>
              </li>
			  <li>
                <a class="c-sidebar__link <?php if(basename($_SERVER['PHP_SELF'])=='advertisement.php'){echo'is-active';}?>" href="advertisement.php">
                  <i class="c-sidebar__icon fa fa-dollar" aria-hidden="true"></i>Monetization
                </a>
              </li>
			   <li>
                <a class="c-sidebar__link <?php if(basename($_SERVER['PHP_SELF'])=='refer.php'){echo'is-active';}?>" href="refer.php">
                  <i class="c-sidebar__icon fa fa-money" aria-hidden="true"></i>Referral Settings
                </a>
              </li>
			   <li>
                <a class="c-sidebar__link <?php if(basename($_SERVER['PHP_SELF'])=='asettings.php'){echo'is-active';}?>" href="asettings.php">
                  <i class="c-sidebar__icon fa fa-cogs" aria-hidden="true"></i>Captcha/Others
                </a>
              </li>
			   <li>
                <a class="c-sidebar__link <?php if(basename($_SERVER['PHP_SELF'])=='o_settings.php'){echo'is-active';}?>" href="o_settings.php">
                <i class="c-sidebar__icon fa fa-mail-forward" aria-hidden="true"></i>Captcha API
                </a>
              </li>
			  <li>
                <a class="c-sidebar__link <?php if(basename($_SERVER['PHP_SELF'])=='password.php'){echo'is-active';}?>" href="password.php">
                  <i class="c-sidebar__icon fa fa-lock" aria-hidden="true"></i>Admin Password
                </a>
              </li>
            </ul>

          </div>
          

          <a class="c-sidebar__footer" href="logout.php">
            Logout <i class="c-sidebar__footer-icon fa fa-sign-out"></i>
          </a>
        </aside>
      </div>
