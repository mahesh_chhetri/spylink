<?php
error_reporting(0);
session_start();
$alert=NULL;

$alert=NULL;
if (isset($_SESSION['admin'])) {
    include '../config.php';
    $id = $_SESSION['admin'];
} else {
    echo "<script type='text/javascript'>
     window.location.href='index.php';
     </script>";
}

$limit = 5;  
if (isset($_GET["page"])) 
{ 
	$page  = $_GET["page"]; 
	
} else { 

$page=1;
 
 };  

 $start_from = ($page-1) * $limit; 


if (isset($_POST['disabled'])) {
    
    $userid = $_POST['id'];
    mysqli_query($conn, "UPDATE users SET status = 'disabled' WHERE user_id ='" . $userid . "'");
    $alert='<div class="alert alert-success">Account Disabled</div>';
}
if (isset($_POST['active'])) {
    
    $userid = $_POST['id'];
    mysqli_query($conn, "UPDATE users SET status = 'active' WHERE user_id ='" . $userid . "'");
    $alert='<div class="alert alert-success">Account Actived</div>';
}

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>All Users</title>
    <meta name="description" content="Neat">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="../assets/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/neat.min.css?v=1.0">
	<script src="assets/js/neat.min.js?v=1.0"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery-1.9.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon">
  </head>
  <body>

    <div class="o-page">
    <?php include 'sidebar.php';?>
      <main class="o-page__content">
        <?php include 'header.php';?>

        <div class="container">
		<?php echo $alert;?>
          <h3>All Users</h3>
		  <br>
	<?php  
$sql = "SELECT * FROM users";  
$rs_result = mysqli_query($conn, $sql);  
$total_records =  mysqli_num_rows($rs_result);  
$total_pages = ceil($total_records / $limit);  
echo "<ul class='pagination'>";
echo "<li class='page-item'><a class='page-link' href='?page=".($page-1)."' class='button'>Previous</a></li>"; 

for ($i=$page; $i<=$page+4; $i++) { 
if($page==$i) {
             echo "<li class='page-item active'><a class='page-link' href='?page=".$i."'>".$i."</a></li>";
			} else {
				echo "<li class='page-item'><a class='page-link' href='?page=".$i."'>".$i."</a></li>"; 
			} 
};  

echo "<li class='page-item'><a class='page-link' href='?page=".($page+1)."' class='button'>NEXT</a></li>";
echo "</ul>";   
?>
	<br>
<div class="c-table-responsive@wide">
<table class="c-table">
    <thead class="c-table__head">
      <tr class="c-table__row">
        <th class="c-table__cell c-table__cell--head">Id</th>
        <th class="c-table__cell c-table__cell--head">Username</th>
        <th class="c-table__cell c-table__cell--head">Email</th>
        <th class="c-table__cell c-table__cell--head">Date</th>
        <th class="c-table__cell c-table__cell--head">Wallet</th>
        <th class="c-table__cell c-table__cell--head">Status</th>
        <th class="c-table__cell c-table__cell--head">Action</th>
      </tr>
    </thead>
    <tbody id="myuserTable">
    <?php
$query   = "select * from users ORDER BY user_id DESC LIMIT $start_from, $limit";
$results = mysqli_query($conn, $query);

while ($row = mysqli_fetch_assoc($results)) {
    if ($row['status'] == 'disabled') {
        $transtatus = 'danger';
        $status     = "Disabled";
    } elseif ($row['status'] == 'inactive') {
        $transtatus = 'warning';
        $status     = "Inactive";
    } else {
        $transtatus = 'success';
        $status     = "Active";
    }
    
    
    echo '
      <tr>
        <td class="c-table__cell">' . $row['user_id'] . '</td>
        <td class="c-table__cell">@' . $row['username'] . '</td>
        <td class="c-table__cell">' . $row['email'] . '</td>
        <td class="c-table__cell">' . $row['date'] . '</td>
        <td class="c-table__cell">' . $currency . '' . $row['wallet'] . '</td>
        <td class="c-table__cell"><span class="badge badge-' . $transtatus . '">' . $status . '</span></td>
        <td class="c-table__cell"><form action="" method="post">
        <input type="hidden" name="id" value="' . $row['user_id'] . '">
        <div class="c-btn--group">
    <input type="submit" name="disabled" value="Disable" class="c-btn c-btn--danger c-btn--small">
    <input type="submit" name="active" value="Active" class="c-btn c-btn--success c-btn--small">
  </div></form></td>
      </tr>';
}
?>
   </tbody>
  </table>
        </div>
        </div>
      </main>
    </div>

    <!-- Main JavaScript -->
    <script src="js/neat.min.js?v=1.0"></script>
  </body>
</html>