<?php
session_start();
$alert=NULL;

$alert=NULL;
if (isset($_SESSION['admin'])) {
    include '../config.php';
    $id = $_SESSION['admin'];
} else {
    echo "<script type='text/javascript'>
     window.location.href='index.php';
     </script>";
}

$settings=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM settings WHERE id='1'"));

if(isset($_POST['update'])){
	$check=mysqli_query($conn,"UPDATE settings SET re_key='".$_POST['re_key']."',re_s_key='".$_POST['re_s_key']."'  WHERE id='1'");
	if($check){
		$alert='<div class="alert alert-success">Captcha Updated</div><meta http-equiv="refresh" content="2">';
	} else {
		$alert='<div class="alert alert-danger">Something is wrong</div><meta http-equiv="refresh" content="2">';
	}
}


?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Captcha Settings</title>
    <meta name="description" content="Neat">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="../assets/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/neat.min.css?v=1.0">
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script src="assets/js/neat.min.js?v=1.0"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery-1.9.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon">
  </head>
  <body>

    <div class="o-page">
    <?php include 'sidebar.php';?>
      <main class="o-page__content">
        <?php include 'header.php';?>

        <div class="container">
		<?php echo $alert;?>
          <h3>Captcha Settings</h3>
		  <br>
		  <div class="c-card">
				<div style="width:100%;" class="card p-4">
					<p>Google ReCaptcha Settings | <span style="color:#00802b;"><b>Note: Use only (reCAPTCHA v2)</b></span></p>
					<br>
					<form action="" method="post">
						<label>Site Key</label>
						<br>
						<input class="form-control" name="re_key" value="<?php echo $settings['re_key'];?>">
						<br>
						<label>Secret Key</label>
						<br>
						<input class="form-control" name="re_s_key" value="<?php echo $settings['re_s_key'];?>">
						<br>
						<input type="submit" name="update" class="c-btn c-btn--warning" value="UPDATE CAPTCHA"/>
					</form>
				</div>
				<br>
				<br>
					<p>Live Test</p>
					<br>
					<div style="width:100px;" class="g-recaptcha" data-sitekey="<?php echo $settings['re_key'];?>"></div>
		  </div>
		  <br>
        </div>
      </main>
    </div>
    <!-- Main JavaScript -->
    <script src="js/neat.min.js?v=1.0"></script>
  </body>
</html>