<?php
session_start();
$alert=NULL;
if (isset($_SESSION['admin'])) {
    include '../config.php';
    $id = $_SESSION['admin'];
} else {
    echo "<script type='text/javascript'>
     window.location.href='index.php';
     </script>";
}

if(isset($_POST['update_app'])){
	$ads1=mysqli_real_escape_string($conn,$_POST['ads1']);
	$ads2=mysqli_real_escape_string($conn,$_POST['ads2']);
	$ads3=mysqli_real_escape_string($conn,$_POST['ads3']);
	
	$check=mysqli_query($conn,"UPDATE ads SET ads1='".$ads1."', ads2='".$ads2."', ads3='".$ads3."' WHERE id='1'");
	if($check){
		$alert='<div class="alert alert-success">Setting Updated</div><meta http-equiv="refresh" content="2">';
	} else {
		$alert='<div class="alert alert-danger">Something is wrong</div><meta http-equiv="refresh" content="2">';
	}
}

if(isset($_POST['update_app_1'])){
	$ads4=mysqli_real_escape_string($conn,$_POST['ads4']);
	$ads5=mysqli_real_escape_string($conn,$_POST['ads5']);
	
	$check=mysqli_query($conn,"UPDATE ads SET ads4='".$ads4."', ads5='".$ads5."' WHERE id='1'");
	if($check){
		$alert='<div class="alert alert-success">Setting Updated</div><meta http-equiv="refresh" content="2">';
	} else {
		$alert='<div class="alert alert-danger">Something is wrong</div><meta http-equiv="refresh" content="2">';
	}
}

$ads_s=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM ads WHERE id='1'"));
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Advertisement</title>
    <meta name="description" content="Neat">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="../assets/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/neat.min.css?v=1.0">
	<script src="assets/js/neat.min.js?v=1.0"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery-1.9.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon">
  </head>
  <body>

    <div class="o-page">
    <?php include 'sidebar.php';?>
      <main class="o-page__content">
        <?php include 'header.php';?>

        <div class="container">
		<?php echo $alert;?>
          <h3>Advertisement Settings</h3>
		  <br>
		  <div class="c-card">
			<div class="row">
				<div class="col-8">
					<p>Settings</p>
					<br>
					<form action="" method="post">
						<label>Header Bar Ad Code</label>
						<br>
						<textarea name="ads1" class="form-control" placeholder="Ads Code"><?php echo $ads_s['ads1'];?></textarea>
						<br>
						<label>Ad Between Page</label>
						<br>
						<textarea name="ads2" class="form-control" placeholder="Ads Code"><?php echo $ads_s['ads2'];?></textarea>
						<br>
						<label>PopUP Ads Code (Will Work In Everypage)</label>
						<br>
						<textarea name="ads3" class="form-control" placeholder="Ads Code"><?php echo $ads_s['ads3'];?></textarea>
						<br>
						<br>
						<input type="submit" name="update_app" class="c-btn c-btn--warning" value="UPDATE"/>
					</form>
				</div>
			</div>
		  </div>
		  <br><div class="c-card">
			<div class="row">
				<div class="col-8">
					<h3>Link Page Ads</h3>
					<br>
					<form action="" method="post">
						<label>Top Ad Code</label>
						<br>
						<textarea name="ads4" class="form-control" placeholder="Ads Code"><?php echo $ads_s['ads4'];?></textarea>
						<br>
						<label>Footer Ad Code</label>
						<br>
						<textarea name="ads5" class="form-control" placeholder="Ads Code"><?php echo $ads_s['ads4'];?></textarea>
						<br>
						<br>
						<input type="submit" name="update_app_1" class="c-btn c-btn--warning" value="UPDATE"/>
					</form>
				</div>
			</div>
		  </div>
        </div>
      </main>
    </div>
<!-- Main JavaScript -->
<script>
$('#insertform').submit(function(){
	return false;
});
$('#send').click(function(){
	$.post(		
		$('#insertform').attr('action'),
		$('#insertform :input').serializeArray(),
		function(result){
			$('#result').html(result);
		}
	);
});

function check(){
	document.getElementById("result").innerHTML="<img style='height:60px;' src='../cdn/loading1.gif'>";
}
</script>
    <script src="js/neat.min.js?v=1.0"></script>
  </body>
</html>