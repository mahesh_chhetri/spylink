<?php
error_reporting(0);
session_start();
$alert=NULL;

$alert=NULL;
if (isset($_SESSION['admin'])) {
    include '../config.php';
    $id = $_SESSION['admin'];
} else {
    echo "<script type='text/javascript'>
     window.location.href='index.php';
     </script>";
}

$limit = 5;  
if (isset($_GET["page"])) 
{ 
	$page  = $_GET["page"]; 
	
} else { 

$page=1;
 
 };  

 $start_from = ($page-1) * $limit; 

$transtype = "pending";
if (isset($_GET['trans'])) {
    if ($_GET['trans'] == 'paid') {
        $transtype = "paid";
    } elseif ($_GET['trans'] == 'cancalled') {
        $transtype = "cancalled";
    } else {
        $transtype = "pending";
    }
}

if (isset($_POST['paid'])) {
    
    $transid = $_POST['id'];
    $usersid = $_POST['userid'];
    mysqli_query($conn, "UPDATE transactions SET status = 'paid' WHERE id ='" . $transid . "'");
    echo "<script type='text/javascript'>
             window.location.href='transactions.php';
             </script>";
}

if (isset($_POST['cancalled'])) {
    $transid    = $_POST['id'];
    $usersid    = $_POST['userid'];
    $useramount = $_POST['amount'];
    mysqli_query($conn, "UPDATE transactions SET status = 'cancalled' WHERE id ='" . $transid . "'");
    mysqli_query($conn, "UPDATE users SET wallet = wallet + $useramount WHERE user_id ='" . $usersid . "'");
    echo "<script type='text/javascript'>
             window.location.href='transactions.php';
             </script>";
}

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>All Transactions</title>
    <meta name="description" content="Neat">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="../assets/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/neat.min.css?v=1.0">
	<script src="assets/js/neat.min.js?v=1.0"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery-1.9.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon">
  </head>
  <body>

    <div class="o-page">
    <?php include 'sidebar.php';?>
      <main class="o-page__content">
        <?php include 'header.php';?>

        <div class="container">
		<?php echo $alert;?>
          <h3>All Withdraw Requests</h3>
		  <br>
		   <div class="btn-group">
   <a href="?trans=paid"><button type="button" class="c-btn c-btn--success c-btn--small">Show Paid</button></a>
   &nbsp;
    <a href="?trans=pending"><button type="button" class="c-btn c-btn--warning c-btn--small">Show Pending</button></a>
	&nbsp;
    <a href="?trans=cancalled"><button type="button" class="c-btn c-btn--danger c-btn--small">Show Cancalled</button></a>
  </div>
		  <br>
		  <br>
	<?php  
$sql = "SELECT * FROM transactions";  
$rs_result = mysqli_query($conn, $sql);  
$total_records =  mysqli_num_rows($rs_result);  
$total_pages = ceil($total_records / $limit);  
echo "<ul class='pagination'>";
echo "<li class='page-item'><a class='page-link' href='?page=".($page-1)."' class='button'>Previous</a></li>"; 

for ($i=$page; $i<=$page+4; $i++) { 
if($page==$i) {
             echo "<li class='page-item active'><a class='page-link' href='?page=".$i."'>".$i."</a></li>";
			} else {
				echo "<li class='page-item'><a class='page-link' href='?page=".$i."'>".$i."</a></li>"; 
			} 
};  

echo "<li class='page-item'><a class='page-link' href='?page=".($page+1)."' class='button'>NEXT</a></li>";
echo "</ul>";   
?>
	<br>
<div class="c-table-responsive@wide">
<table class="c-table">
    <thead class="c-table__head">
      <tr class="c-table__row">
		<th class="c-table__cell c-table__cell--head">ID</th>
        <th class="c-table__cell c-table__cell--head">Date</th>
        <th class="c-table__cell c-table__cell--head">Username</th>
        <th class="c-table__cell c-table__cell--head">Payment Amount</th>
        <th class="c-table__cell c-table__cell--head">Payment Method</th>
        <th class="c-table__cell c-table__cell--head">Status</th>
        <th class="c-table__cell c-table__cell--head">Action</th>
      </tr>
    </thead>
    <tbody id="myuserTable">
    <?php
$query   = "select * from transactions WHERE status='$transtype' ORDER BY id DESC LIMIT $start_from, $limit";
$results = mysqli_query($conn, $query);

while ($row = mysqli_fetch_assoc($results)) {
     if ($row['status'] == 'pending') {
        $transtatus = 'warning';
    } elseif ($row['status'] == 'paid') {
        $transtatus = 'success';
    } else {
        $transtatus = 'danger';
    }
    
    $users=mysqli_fetch_array(mysqli_query($conn,"SELECT * from users where user_id='".$row['user_id']."'"));
    echo '
      <tr>
        <td class="c-table__cell">' . $row['id'] . '</td>
        <td class="c-table__cell">' . date('d M Y',strtotime($row['date'])) . '</td>
        <td class="c-table__cell">@' . $users['username'] . '</td>
        <td class="c-table__cell">' . $currency . '' . $row['amount'] . '</td>
        <td class="c-table__cell">(' . $users['pay_method'] . ') '.$users['details'].'</td>
        <td class="c-table__cell"><span class="badge badge-' . $transtatus . '">' . $row['status'] . '</span></td>
        <td class="c-table__cell"><form action="" method="post"><input type="hidden" name="id" value="' . $row['id'] . '"><input type="hidden" name="userid" value="' . $row['user_id'] . '"><input type="hidden" name="amount" value="' . $row['amount'] . '"><div class="btn-group">
    <input type="submit" name="paid" value="Paid" class="c-btn c-btn--success c-btn--small">
	&nbsp;
    <input type="submit" name="cancalled" value="Cancal" class="c-btn c-btn--danger c-btn--small">
  </div></form></td>
      </tr>';
}
?>
   </tbody>
  </table>
        </div>
        </div>
      </main>
    </div>

    <!-- Main JavaScript -->
    <script src="js/neat.min.js?v=1.0"></script>
  </body>
</html>