<?php
error_reporting(0);
session_start();
$alert=NULL;

$alert=NULL;
if (isset($_SESSION['admin'])) {
    include '../config.php';
    $id = $_SESSION['admin'];
} else {
    echo "<script type='text/javascript'>
     window.location.href='index.php';
     </script>";
}

$limit = 10;  
if (isset($_GET["page"])) 
{ 
	$page  = $_GET["page"]; 
	
} else { 

$page=1;
 
 };  

 $start_from = ($page-1) * $limit; 


	if(isset($_POST['block'])){
		$f=mysqli_query($conn,"UPDATE links SET status='blocked' WHERE id='".$_POST['id']."'");
		if($f) {
		$alert='<div class="alert alert-success"><i class="fa fa-lock"></i>&nbsp;&nbsp;Link Blocked</div>';
		} else {
			echo "error: ".mysqli_error($conn);
		}
	}
	
	if(isset($_POST['unblock'])){
		mysqli_query($conn,"UPDATE links SET status='active' WHERE id='".$_POST['id']."'");
		$alert='<div class="alert alert-success"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Link Actived</div>';
	}
	
	if(isset($_POST['remove'])){
		mysqli_query($conn,"UPDATE links SET status='removed' WHERE id='".$_POST['id']."'");
		$alert='<div class="alert alert-success"><i class="fa fa-remove"></i>&nbsp;&nbsp;Link Removed</div>';
	}

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>All Links</title>
    <meta name="description" content="Neat">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="../assets/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/neat.min.css?v=1.0">
	<script src="assets/js/neat.min.js?v=1.0"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery-1.9.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon">
  </head>
  <body>

    <div class="o-page">
    <?php include 'sidebar.php';?>
      <main class="o-page__content">
        <?php include 'header.php';?>

        <div class="container">
		<?php echo $alert;?>
          <h3>All Links</h3>
		  <br>

	<?php  
$sql = "SELECT * FROM links";  
$rs_result = mysqli_query($conn, $sql);  
$total_records =  mysqli_num_rows($rs_result);  
$total_pages = ceil($total_records / $limit);  
echo "<ul class='pagination'>";
echo "<li class='page-item'><a class='page-link' href='?page=".($page-1)."' class='button'>Previous</a></li>"; 

for ($i=$page; $i<=$page+4; $i++) { 
if($page==$i) {
             echo "<li class='page-item active'><a class='page-link' href='?page=".$i."'>".$i."</a></li>";
			} else {
				echo "<li class='page-item'><a class='page-link' href='?page=".$i."'>".$i."</a></li>"; 
			} 
};  

echo "<li class='page-item'><a class='page-link' href='?page=".($page+1)."' class='button'>NEXT</a></li>";
echo "</ul>";   
?>
	<br>
<div class="c-table-responsive@wide">
<table class="c-table">
    <thead class="c-table__head">
      <tr class="c-table__row">
        <th class="c-table__cell c-table__cell--head">Id</th>
        <th class="c-table__cell c-table__cell--head">Link ID</th>
        <th class="c-table__cell c-table__cell--head">Remove ID</th>
        <th class="c-table__cell c-table__cell--head">Created on</th>
        <th class="c-table__cell c-table__cell--head">Created by</th>
        <th class="c-table__cell c-table__cell--head">Status</th>
        <th class="c-table__cell c-table__cell--head">Action</th>
      </tr>
    </thead>
    <tbody id="myuserTable">
    <?php
$query   = "select * from links ORDER BY id DESC LIMIT $start_from, $limit";
$results = mysqli_query($conn, $query);

while ($row = mysqli_fetch_assoc($results)) {
    if ($row['status'] == 'blocked') {
        $transtatus = 'danger';
        $status     = "Blocked";
    } elseif ($row['status'] == 'removed') {
        $transtatus = 'warning';
        $status     = "removed";
    } else {
        $transtatus = 'success';
        $status     = "Active";
    }
    
	$users=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM users WHERE user_id='".$row['user_id']."'"));
    if($row['user_id']==''){
		$username='<i class="fa fa-user"></i>&nbsp;&nbsp;Anonymous';
	} else {
		$username='@' . $users['username'] . '';
	}
	
    echo '
      <tr>
        <td class="c-table__cell">' . $row['id'] . '</td>
        <td class="c-table__cell">' . $row['link_id'] . '</td>
        <td class="c-table__cell">' . $row['remove_id'] . '</td>
        <td class="c-table__cell">' . date("d M Y",strtotime($row['date'])) . '</td>
        <td class="c-table__cell">'.$username.'</td>
        <td class="c-table__cell"><span class="badge badge-' . $transtatus . '">' . $status . '</span></td>
        <td class="c-table__cell">
		<form action="" method="post">
       <input type="hidden" name="id" value="'.$row['id'].'"/>
	   <a class="c-btn c-btn--success c-btn--small" href="edit_links.php?id='.$row['id'].'"><i class="fa fa-pencil"></i></a>
		<a class="c-btn c-btn--warning c-btn--small" target="_blank" href="../views/'.$row['link_id'].'"><i class="fa fa-eye"></i></a>
		<button name="block" class="c-btn c-btn--dark c-btn--small"><i class="fa fa-lock"></i></button>
		<button name="unblock" class="c-btn c-btn--success c-btn--small"><i class="fa fa-unlock"></i></button>
		<button name="remove" class="c-btn c-btn--danger c-btn--small"><i class="fa fa-remove"></i></button>
  </form></td>
      </tr>';
}
?>
   </tbody>
  </table>
  <br>
  <br>
        </div>
        </div>
      </main>
    </div>
<br>
<br>
<br>
    <!-- Main JavaScript -->
    <script src="js/neat.min.js?v=1.0"></script>
  </body>
</html>