<?php
error_reporting(0);
session_start();
$alert=NULL;

$alert=NULL;
if (isset($_SESSION['admin'])) {
    include '../config.php';
    $id = $_SESSION['admin'];
} else {
    echo "<script type='text/javascript'>
     window.location.href='index.php';
     </script>";
}

if(isset($_POST['update'])){
	$urls=mysqli_real_escape_string($conn,$_POST['urls']);
	$password=mysqli_real_escape_string($conn,$_POST['password']);
	$title=mysqli_real_escape_string($conn,$_POST['title']);
	$m=mysqli_query($conn,"UPDATE links SET urls='".$urls."', pass='".$_POST['pass']."', password='".$password."', title='".$title."', captcha='".$_POST['captcha']."', status='".$_POST['status']."' WHERE id='".$_GET['id']."'");
	if($m){
		$alert='<div class="alert alert-success">Updated</div>';
	} else {
		$alert='<div class="alert alert-danger">Failed, '.mysqli_error($conn).'</div>';
	}
}

$links=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM links WHERE id='".$_GET['id']."'"));

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>All Links</title>
    <meta name="description" content="Neat">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="../assets/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/neat.min.css?v=1.0">
	<script src="assets/js/neat.min.js?v=1.0"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery-1.9.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon">
  </head>
  <body>

    <div class="o-page">
    <?php include 'sidebar.php';?>
      <main class="o-page__content">
        <?php include 'header.php';?>

        <div class="container">
		<?php echo $alert;?>
          <h3>Edit Link</h3>
		  <br>
		  <div class="c-card">
		  <form action="" method="post">
			<label>URLs</label>
			<textarea style="height:200px;" name="urls" class="form-control" placeholder="List of Urls"><?php echo $links['urls'];?></textarea>
			<br>
			<label>Status</label>
			<select name="status" class="form-control">
							<option value="active" <?php if($links['status']=='active'){echo 'selected';}else{echo'';}?>>Active</option>
							<option value="blocked" <?php if($links['status']=='blocked'){echo 'selected';}else{echo'';}?>>Blocked</option>
							<option value="removed" <?php if($links['status']=='removed'){echo 'selected';}else{echo'';}?>>Removed</option>
			</select>
			<br>
					<div class="form-check-inline">
					  <label class="form-check-label">
						<span class="badge badge-success">Captcha: </span>
					  </label>
					</div>
					<div class="form-check-inline">
					  <label class="form-check-label">
						<input type="radio" class="form-check-input" name="captcha" value="on" <?php if($links['captcha']=='on'){echo 'checked';}else{echo'';}?>>On
					  </label>
					</div>
					<div class="form-check-inline">
					  <label class="form-check-label">
						<input type="radio" class="form-check-input" name="captcha" value="off" <?php if($links['captcha']=='off'){echo 'checked';}else{echo'';}?>>Off
					  </label>
					</div>
					<br>
					<br>
					<div class="form-check-inline">
					  <label class="form-check-label">
						<span class="badge badge-success">Encyption Key: </span>
					  </label>
					</div>
					<div class="form-check-inline">
					  <label class="form-check-label">
						<input type="radio" class="form-check-input" name="pass" value="yes" <?php if($links['pass']=='yes'){echo 'checked';}else{echo'';}?>>On
					  </label>
					</div>
					<div class="form-check-inline">
					  <label class="form-check-label">
						<input type="radio" class="form-check-input"  name="pass" value="no" <?php if($links['pass']=='no'){echo 'checked';}else{echo'';}?>>Off
					  </label>
					</div>
					<br>
					<br>
				    <input name="password" class="form-control" id="password" placeholder="Encryption Password" value="<?php echo $links['password'];?>">
				    <br>
				     <input  name="title" class="form-control" placeholder="Title" value="<?php echo $links['title'];?>">
				<br>
				<input name="update" type="submit" class="c-btn c-btn--success" value="UPDATE"/>
				</form>
		  </div>
		<br>
			
        </div>
      </main>
    </div>
<br>
<br>
<br>
    <!-- Main JavaScript -->
    <script src="js/neat.min.js?v=1.0"></script>
	<script>
		/*var check=document.getElementsByName("pass")[0].value;
		alert(check);
		if(check=='yes'){
			document.getElementById("password").disabled=false;
		} else {
			document.getElementById("password").disabled=true;
		}
		
		$('#key').click(function(){
			document.getElementById("password").disabled=false;
		});
		$('#key1').click(function(){
			document.getElementById("password").disabled=true;
		}); */
	</script>
  </body>
</html>