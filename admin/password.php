<?php
session_start();
$alert=NULL;

$alert=NULL;
if (isset($_SESSION['admin'])) {
    include '../config.php';
    $id = $_SESSION['admin'];
} else {
    echo "<script type='text/javascript'>
     window.location.href='index.php';
     </script>";
}
if(isset($_POST['save'])){
		$settings=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM admin where id=1"));
		if($settings['password']==$_POST['cr_password']){
			$username=mysqli_real_escape_string($conn,$_POST['username']);
			$password=mysqli_real_escape_string($conn,$_POST['password']);
			$update=mysqli_query($conn,"UPDATE admin SET username='".$username."', password='".$password."' WHERE id=1");
			if($update){
				$alert='<div class="alert alert-success">Settings Updated</div>';
			} else{
				$alert='<div class="alert alert-danger">Something is wrong</div>';
			}
		} else {
			$alert='<div class="alert alert-info">Incorrect Corrent Password</div>';
		}
	}

$admin_data=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM admin WHERE id='1'"));
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Admin Password</title>
    <meta name="description" content="Neat">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="../assets/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/neat.min.css?v=1.0">
	<script src="assets/js/neat.min.js?v=1.0"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery-1.9.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon">
  </head>
  <body>

    <div class="o-page">
    <?php include 'sidebar.php';?>
      <main class="o-page__content">
        <?php include 'header.php';?>

        <div class="container">
		<?php echo $alert;?>
          <h3>Username and Password Settings</h3>
		  <br>
		  <div class="c-card">
<div class="row">
                                <div class="col-sm">
                                    <form  action="" method="post">
										 <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputuname_1">Enter New Username</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                                                </div>
                                                <input type="text" name="username" class="form-control" id="exampleInputuname_1" placeholder="Username" value="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1">Enter New Password</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-lock"></i></span>
                                                </div>
                                                <input type="text" name="password" class="form-control" placeholder="Enter Password" value="">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1">Current Password</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-lock"></i></span>
                                                </div>
                                                <input type="text" name="cr_password" class="form-control" placeholder="Enter Password" value="">
                                            </div>
                                        </div>
                                        <input name="save" type="submit" class="btn btn-primary" value="UPDATE">
                                    </form>
                                </div>
                            </div>
		  </div>
        </div>
      </main>
    </div>

    <!-- Main JavaScript -->
    <script src="js/neat.min.js?v=1.0"></script>
  </body>
</html>