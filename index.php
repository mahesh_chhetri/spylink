<?php
session_start();
include 'config.php';
$settings=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM settings WHERE id=1"));
?>

<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="full/img/favicon.png" type="image/png">
	<title><?=$settings['site_name']?></title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="themeV4/landingPage/css/bootstrap.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/linericon/style.css">
	<link rel="stylesheet" href="themeV4/landingPage/css/font-awesome.min.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/owl-carousel/owl.carousel.min.css">
	<link rel="stylesheet" href="themeV4/landingPage/css/magnific-popup.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/nice-select/css/nice-select.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/animate-css/animate.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/flaticon/flaticon.css">
	<!-- main css -->
	<link rel="stylesheet" href="themeV4/landingPage/css/style.css">
</head>

<body>
	<!--================Header Menu Area =================-->
	<?php include 'themeV4/partials/header-landing.php'?>
	<!--================Header Menu Area =================-->

	<!--================Home Banner Area =================-->
	<section class="home_banner_area">
		<div class="banner_inner">
			<div class="container">
				<div class="row">
					<div class="col-lg-5">
						<div class="banner_content">
							<h2>Protect Links
								<br>
								And Earn Money
							</h2>
							<p>
								Earn Money By Protecting Your Links<br>
								Safe Your All Links in One Place
							</p>
							<div class="d-flex align-items-center">
								<a class="primary_btn" href="protect.php"><span>Get Started</span></a>
								<a id="play-home-video" class="video-play-button"
									href="https://www.youtube.com/watch?time_continue=2&v=J9YzcEe29d0">
									<span></span>
								</a>
								<div class="watch_video text-uppercase">
									watch the video
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-7">
						<div class="home_right_img">
							<img class="img-fluid" src="themeV4/landingPage/img/banner/home-right.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->

	<!--================Start Features Area =================-->
	<section class="section_gap features_area">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8 text-center">
					<div class="main_title">
						<p class="top_title">HIGHEST CPM IN INDIA</p>
						<h2>Powered by afly </h2>
						<p>Indian Best & Trusted AFLY INDIA</p>
						<a href="signup.php" class="primary_btn"><span>Start Free Now!</span></a>
					</div>
				</div>
			</div>
			<div class="row align-items-center">
				<div class="col-lg-6">
					<div class="left_features">
						<img class="img-fluid" src="themeV4/landingPage/img/earnmoney.jpg" alt="">
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1">
					<!-- single features -->
					<div class="single_feature">
						<div class="feature_head">
							<span class="lnr lnr-screen"></span>
							<h4>CREATE ACCOUNT</h4>
						</div>
						<div class="feature_content">
							<p>Just Click on Signup button and Create Your Account for Free.</p>
						</div>
					</div>
					<!-- single features -->
					<div class="single_feature">
						<div class="feature_head">
							<span class="lnr lnr-screen"></span>
							<h4>PROTECT YOUR ALL LINKS</h4>
						</div>
						<div class="feature_content">
							<p>Proctect Multipal Links in One Page
								and Share with Your user</p>
						</div>
					</div>
					<!-- single features -->
					<div class="single_feature">
						<div class="feature_head">
							<span class="lnr lnr-screen"></span>
							<h4>EARN MONEY</h4>
						</div>
						<div class="feature_content">
							<p>When You Share Your Links With Your user for every 1000 visitor you will get upto $1</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Features Area =================-->

	<!--================Recent Update Area =================-->
	<section class="recent_update_area">
		<div class="container">
			<div class="recent_update_inner">
				<ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
							aria-controls="home" aria-selected="true">
							<span class="lnr lnr-screen"></span>
							<h6>EARN AT HOME</h6>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
							aria-controls="profile" aria-selected="false">
							<span class="lnr lnr-screen"></span>
							<h6>FAST PAYMENT</h6>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
							aria-controls="contact" aria-selected="false">
							<span class="lnr lnr-screen"></span>
							<h6>24*7 SUPPORT</h6>
						</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
						<div class="row recent_update_text align-items-center">
							<div class="col-lg-5">
								<div class="common_style">
									<p class="line">EARN MONEY FROM HOME</p>
									<h3>We Believe that <br />Everyone have Right To Earn in Online</h3>
									<p>Now You can Earn from home using spylink.in</p>
									<a class="primary_btn" href="#"><span>Learn More</span></a>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="chart_img">
									<img class="img-fluid" src="themeV4/landingPage/img/earnathome.jpg" alt="">
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
						<div class="row recent_update_text align-items-center">
							<div class="col-lg-5">
								<div class="common_style">
									<p class="line">PAYMENT SYSTEM</p>
									<h3>Get Your Payment <br />within 24 hours </h3>
									<p>PAYTM,PhonePe,UPI,PayPal,Bank Transfer</p>
									<a class="primary_btn" href="#"><span>Learn More</span></a>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="chart_img">
									<img class="img-fluid" src="themeV4/landingPage/img/payment.jpg" alt="">
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade show active" id="contact" role="tabpanel" aria-labelledby="contact-tab">
						<div class="row recent_update_text align-items-center">
							<div class="col-lg-5">
								<div class="common_style">
									<p class="line">24*7 WhatsApp</p>
									<h3>We Believe that <br />Your Problem is
										priority</h3>
									<p>Our Support Team is Available 24*7 For Your Help </p>
									<a class="primary_btn" href="#"><span>Learn More</span></a>
								</div>
							</div>
							<div class="col-lg-6 text-right">
								<div class="chart_img">
									<img class="img-fluid" src="themeV4/landingPage/img/support.jpg" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================ Recent Update Area =================-->

	<!--================Start Big Features Area =================-->
	<section class="section_gap big_features">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8 text-center">
					<div class="main_title">
						<p class="top_title">Features Specifications</p>
						<h2>Amazing Features That make it Awesome!</h2>
						<p>UNIQUE DESINE TRACK YOUR EARNING AND REPORT EASY WITHDRWAL OPTION </p>
					</div>
				</div>
			</div>
			<div class="row features_content">
				<div class="col-lg-4 offset-lg-1">
					<div class="big_f_left">
						<img class="img-fluid" src="themeV4/landingPage/img/f-img1.png" alt="">
					</div>
				</div>
				<div class="col-lg-4 offset-lg-2">
					<div class="common_style">
						<p class="line">SAFE & 100% TRUSTED IN INDIA</p>
						<h3>We Believe that <br />We Believe that
							Online Earning is also Risk Because of Fraud Site but spylink is 100& safe.</p>
							<a class="primary_btn" href="#"><span>Learn More</span></a>
					</div>
				</div>
				<div class="border-line"></div>
				<img class="shape1" src="themeV4/landingPage/img/shape1.png" alt="">
				<img class="shape2" src="themeV4/landingPage/img/shape2.png" alt="">
				<img class="shape3" src="themeV4/landingPage/img/shape1.png" alt="">
			</div>

			<div class="row features_content bottom-features">
				<div class="col-lg-5">
					<div class="common_style">
						<p class="line">Powered by afly</p>
						<h3>We Believe that <br />Trust is Important then other things</h3>
						<p>AFLY INDIA PAY 1500 PLUS USER WITHIN 2 YEAR</p>
						<a class="primary_btn" href="#"><span>Learn More</span></a>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-2">
					<div class="big_f_left">
						<img class="img-fluid" src="themeV4/landingPage/img/f-img2.png" alt="">
					</div>
				</div>
				<div class="border-line"></div>
				<img class="shape1" src="themeV4/landingPage/img/shape1.png" alt="">
				<img class="shape2" src="themeV4/landingPage/img/shape2.png" alt="">
				<img class="shape3" src="themeV4/landingPage/img/shape1.png" alt="">
			</div>
		</div>
	</section>
	<!--================End Big Features Area =================-->

	<!--================Srart Pricing Area =================-->

	<!--================ End Testimonial Area =================-->

	<!--================ Srart Brand Area =================-->

	<!--================ End Brand Area =================-->

	<!--================Impress Area =================-->
	<section class="impress_area">
		<div class="container">
			<div class="impress_inner">
				<h2>Got Impressed to our features?</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
					et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
				<a class="primary_btn" href="#"><span>Request Free Demo</span></a>
			</div>
		</div>
	</section>
	<!--================End Impress Area =================-->


	<!--================Footer Area =================-->
	<?php include 'themeV4/partials/footer-landing.php'?>
	<!--================End Footer Area =================-->

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="themeV4/landingPage/js/jquery-3.2.1.min.js"></script>
	<script src="themeV4/landingPage/js/popper.js"></script>
	<script src="themeV4/landingPage/js/bootstrap.min.js"></script>
	<script src="themeV4/landingPage/js/stellar.js"></script>
	<script src="themeV4/landingPage/js/jquery.magnific-popup.min.js"></script>
	<script src="themeV4/landingPage/vendors/nice-select/js/jquery.nice-select.min.js"></script>
	<script src="themeV4/landingPage/vendors/isotope/imagesloaded.pkgd.min.js"></script>
	<script src="themeV4/landingPage/vendors/isotope/isotope-min.js"></script>
	<script src="themeV4/landingPage/vendors/owl-carousel/owl.carousel.min.js"></script>
	<script src="themeV4/landingPage/js/jquery.ajaxchimp.min.js"></script>
	<script src="themeV4/landingPage/vendors/counter-up/jquery.waypoints.min.js"></script>
	<script src="themeV4/landingPage/vendors/counter-up/jquery.counterup.min.js"></script>
	<script src="themeV4/landingPage/js/mail-script.js"></script>
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="themeV4/landingPage/js/gmaps.min.js"></script>
	<script src="themeV4/landingPage/js/theme.js"></script>
</body>

</html>