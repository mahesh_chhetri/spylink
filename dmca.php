<?php
	session_start();
	include 'config.php';
	$alert=NULL;
	$settings=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM settings WHERE id='1'"));
	$ads=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM ads WHERE id='1'"));
?>

<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="full/img/favicon.png" type="image/png">
	<title>DMCA Policy | <?php echo $settings['site_name'];?></title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="themeV4/landingPage/css/bootstrap.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/linericon/style.css">
	<link rel="stylesheet" href="themeV4/landingPage/css/font-awesome.min.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/owl-carousel/owl.carousel.min.css">
	<link rel="stylesheet" href="themeV4/landingPage/css/magnific-popup.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/nice-select/css/nice-select.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/animate-css/animate.css">
	<link rel="stylesheet" href="themeV4/landingPage/vendors/flaticon/flaticon.css">
	<!-- main css -->
	<link rel="stylesheet" href="themeV4/landingPage/css/style.css">
</head>

<body>
	<!--================Header Menu Area =================-->
	<?php include 'themeV4/partials/header-landing.php'?>
	<!--================Header Menu Area =================-->

	<!--================End Home Banner Area =================-->

	<!--================End Features Area =================-->

	<!--================ Recent Update Area =================-->

	<!--================Start Big Features Area =================-->
	<section class="section_gap big_features">
		<div class="container">
			<div class="row features_content bottom-features">
				<div class="col-lg-12">
				<div class="card p-4">
			<h5><i style="color:green;" class="fa fa-file-text-o"></i>&nbsp;Digital Millennium Copyright Act (DMCA)</h5>
			<hr>
			<?php echo $alert;?>
			<div class="page-content">
			<?php
	if($check_ip>=2){
		echo '';
	} else {
		echo '
			<form id="insertformMoney" action="ad_click.php" method="post">
			<input type="hidden" name="ip" value="'.$users_ip.'"/>
			<div id="sendMoney">
				<div id="resultMoney">'.$ads['ads2'].'</h4>
			</div>
			</form>
		';
	}
?>
				<p> <strong><?php echo $_SERVER['HTTP_HOST'];?></strong>  intends to fully comply with the Digital Millennium Copyright Act ("DMCA"), including the notice and "take down" Provisions, and to benefit from the safe harbors immunizing  <strong><?php echo $_SERVER['HTTP_HOST'];?></strong>  from liability to the fullest extent of the law.  <strong><?php echo $_SERVER['HTTP_HOST'];?></strong>  reserves the right to terminate the account of any Member who infringes upon the copyright rights of others upon receipt of proper notification by the copyright owner or the copyright owner's legal agent. Included below are the processes and procedures that  <strong><?php echo $_SERVER['HTTP_HOST'];?></strong>  will follow to resolve any claims of intellectual property violations.

It’s against our policies/terms to post copyrighted material you don’t have the authorization to use. If anybody find any link on our site which violate our terms in any case than report us asap</p>
		</div>
		</div>
		<br>
		</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Big Features Area =================-->

	<!--================Srart Pricing Area =================-->

	<!--================ End Testimonial Area =================-->

	<!--================ Srart Brand Area =================-->

	<!--================ End Brand Area =================-->



	<!--================Footer Area =================-->
	<?php include 'themeV4/partials/footer-landing.php'?>
	<!--================End Footer Area =================-->

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="themeV4/landingPage/js/jquery-3.2.1.min.js"></script>
	<script src="themeV4/landingPage/js/popper.js"></script>
	<script src="themeV4/landingPage/js/bootstrap.min.js"></script>
	<script src="themeV4/landingPage/js/stellar.js"></script>
	<script src="themeV4/landingPage/js/jquery.magnific-popup.min.js"></script>
	<script src="themeV4/landingPage/vendors/nice-select/js/jquery.nice-select.min.js"></script>
	<script src="themeV4/landingPage/vendors/isotope/imagesloaded.pkgd.min.js"></script>
	<script src="themeV4/landingPage/vendors/isotope/isotope-min.js"></script>
	<script src="themeV4/landingPage/vendors/owl-carousel/owl.carousel.min.js"></script>
	<script src="themeV4/landingPage/js/jquery.ajaxchimp.min.js"></script>
	<script src="themeV4/landingPage/vendors/counter-up/jquery.waypoints.min.js"></script>
	<script src="themeV4/landingPage/vendors/counter-up/jquery.counterup.min.js"></script>
	<script src="themeV4/landingPage/js/mail-script.js"></script>
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="themeV4/landingPage/js/gmaps.min.js"></script>
	<script src="themeV4/landingPage/js/theme.js"></script>

	<script>
		$('#insertformMoney').submit(function(){
			return false;
		});
		$('#sendMoney').click(function(){
			$.post(		
				$('#insertformMoney').attr('action'),
				$('#insertformMoney :input').serializeArray(),
				function(result){
					$('#resultMoney').html(result);
				}
			);
		});
			$('#insertformHead').submit(function(){
			return false;
		});
		$('#sendHead').click(function(){
			$.post(		
				$('#insertformHead').attr('action'),
				$('#insertformHead :input').serializeArray(),
				function(result){
					$('#resultHead').html(result);
				}
			);
		});
		
		$('#insertformFoot').submit(function(){
			return false;
		});
		$('#sendFoot').click(function(){
			$.post(		
				$('#insertformFoot').attr('action'),
				$('#insertformFoot :input').serializeArray(),
				function(result){
					$('#resultFoot').html(result);
				}
			);
		});
	</script>
</body>

</html>