'use strict';
/**
 * Script responsible for login pag only
 */
$(function () {
    /**
     * Toggle password field for view/hide
     */
    $(".view-password").on('click', function () {
        var attr = document.querySelector('#password');
        var attrValue = attr.type;
        if (attrValue === 'password') {
            attr.setAttribute('type', 'text');
        } else {
            attr.setAttribute('type', 'password');
        }
    });
});