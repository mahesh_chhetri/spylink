<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright © <a href="https://spylink.in/">SpyLink</a> <?=Date('Y')?></span>
        </div>
    </div>
</footer>