<?php
if(!$_SESSION['user_id']){
	echo '<script>
    window.location = "logout.php";
</script>';
} 
date_default_timezone_set(date_default_timezone_get());
function generateRandomString($length = 100) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}			
$token=generateRandomString();
			
$users_status=mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM users WHERE user_id='".$_SESSION['user_id']."'"));
if($user_status['status']=='deactive'){
		mysqli_query($conn,"UPDATE users SET token='".$token."', last_login='". date("Y/m/d H:i:s")."' WHERE user_id='".$_SESSION['user_id']."'");
		header('location: confirm.php?token='.$token.'');
} else if ($user_status=='disabled'){
	header('location:logout.php');
} else {}
?>

<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </button>
    <a class="navbar-brand" href="/">
        <img style="height:50px;" class="brand-img d-inline-block"
            src="dist/img/logo-light.png" alt="brand">
    </a>
    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">
        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <!-- Dropdown - Messages -->
        <!-- Nav Item - Alerts -->
        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-bell fa-fw" aria-hidden="true"></i>
                <!-- Counter - Alerts -->
                <span class="badge badge-danger badge-counter">
                    <div id="sendHead">
                        <div id="resultHead">2+</div>
                    </div>
                </span>
            </a>
            <!-- Dropdown - Alerts -->
            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                    Alerts Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                    <div class="mr-3">
                        <div class="icon-circle bg-primary">
                            <i class="fa fa-file text-white" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div>
                        <div class="small text-gray-500">
                            <div id="sendHead">
                                <div id="resultHead">June 7, 2020</div>
                            </div>

                        </div>
                        <span class="font-weight-bold">
                            <div id="sendHead">
                                <div id="resultHead">Something New Is Coming ;)</div>
                            </div>

                        </span>
                    </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                    <div class="mr-3">
                        <div class="icon-circle bg-success">
                            <i class="fa fa-usd text-white" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div>
                        <div class="small text-gray-500">
                            <div id="sendHead">
                                <div id="resultHead">June 1, 2020</div>
                            </div>

                        </div>

                        <div id="sendHead">
                            <div id="resultHead">All Payment Is Done</div>
                        </div>


                    </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
            </div>
        </li>

        <!-- Nav Item - Messages -->

        <!-- Dropdown - Messages -->
        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
            aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">
                Message Center
            </h6>
            <a class="dropdown-item d-flex align-items-center" href="#">
                <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                </div>
                <div class="font-weight-bold">
                    <div class="text-truncate">Hi there! I am wondering if you can help me with a
                        problem I've been having.</div>
                    <div class="small text-gray-500">Emily Fowler · 58m</div>
                </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
                <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                    <div class="status-indicator"></div>
                </div>
                <div>
                    <div class="text-truncate">I have the photos that you ordered last month, how would
                        you like them sent to you?</div>
                    <div class="small text-gray-500">Jae Chun · 1d</div>
                </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
                <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                    <div class="status-indicator bg-warning"></div>
                </div>
                <div>
                    <div class="text-truncate">Last month's report looks great, I am very happy with the
                        progress so far, keep up the good work!</div>
                    <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
                <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                </div>
                <div>
                    <div class="text-truncate">Am I a good boy? The reason I ask is because someone told
                        me that people say this to all dogs, even if they aren't good...</div>
                    <div class="small text-gray-500">Chicken the Dog · 2w</div>
                </div>
            </a>
            <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
        </div>


        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">

                    PROFILE

                </span>
                <img class="img-profile rounded-circle" src="https://img.icons8.com/color/2x/user-male-circle.png">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="account">
                    <i class="fa fa-user fa-sm fa-fw mr-2 text-gray-400" aria-hidden="true"></i>
                    Profile
                </a>
                <a class="dropdown-item" href="password">
                    <i class="fa fa-cogs fa-sm fa-fw mr-2 text-gray-400" aria-hidden="true"></i>
                    Settings
                </a>
                <a class="dropdown-item" href="payment">
                    <i class="fa fa-list fa-sm fa-fw mr-2 text-gray-400" aria-hidden="true"></i>
                    Payment Settings
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="logout" data-toggle="modal" data-target="#logoutModal">
                    <i class="fa fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400" aria-hidden="true"></i>
                    Logout
                </a>
            </div>
        </li>

    </ul>

</nav>