<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
    <div class="sidebar-brand-icon">
    <img src="dist/img/logo-s.png" />
    </div>
    <div class="sidebar-brand-text mx-3"><?=$settings['site_name']?></div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="dashboard">
      <span class="feather-icon"> <i class="fas fa-fw fa-tachometer-alt" aria-hidden="true"></i></span>
      <span class="nav-link-text">Dashboard</span>
    </a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Manage
  </div>

  <!-- Nav Item - Pages Collapse Menu -->

  <li class="nav-item">
    <a class="nav-link" href="protect">
      <i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;Protect</a>
  </li>

  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
      aria-controls="collapseTwo">
      <i class="fa fa-fw fa-cog" aria-hidden="true"></i>
      <span>Manage Links</span>
    </a>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Custom Components:</h6>
        <a class="collapse-item" href="all_links?s=active">Active</a>
        <a class="collapse-item" href="all_links?s=blocked">Blocked</a>
        <a class="collapse-item" href="all_links?s=removed">Removed</a>
        <a class="collapse-item" href="links"> Manage Links</a>
      </div>
    </div>
  </li>

  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true"
      aria-controls="collapseUtilities">
      <i class="fa fa-money" aria-hidden="true"></i>
      <span>Money Withdraw</span>
    </a>
    <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Custom Utilities:</h6>
        <a class="collapse-item" href="withdraw?s=paid">Paid</a>
        <a class="collapse-item" href="withdraw?s=pending">Pending</a>
        <a class="collapse-item" href="withdraw?s=cancalled">Cancalled</a>
      </div>
    </div>
  </li>

  <!-- <li class="nav-item ">
    <a class="nav-link" href="Report">
      <i class="fas fa-tools" aria-hidden="true"></i>
      <span class="nav-link-text">Full Report</span>
    </a>
  </li> -->


  <li class="nav-item">

  </li>
  <li class="nav-item ">
    <a class="nav-link" href="refer">
      <i class="fa fa-users" aria-hidden="true"></i>
      <span class="nav-link-text">Refer &amp; Earn</span>
    </a>
  </li>



  <!-- Nav Item - Utilities Collapse Menu -->


  <li class="nav-item ">
    <a class="nav-link" href="mywallet">
      <i class="fa fa-money" aria-hidden="true"></i>
      <span class="nav-link-text">My Wallet</span>
    </a>
  </li>
  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Settings
  </div>

  <!-- Nav Item - Pages Collapse Menu -->
  <ul class="navbar-nav flex-column">
    <li class="nav-item ">
      <a class="nav-link" href="account">
        <span class="feather-icon"> <i class="fa fa-fw fa-cog" aria-hidden="true"></i></span>
        <span class="nav-link-text">Account Settings</span>
      </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link " href="password">
        <i class="fa fa-lock" aria-hidden="true"></i>
        <span class="nav-link-text">Change Password</span>
      </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="payment">
        <i class="fa fa-exchange" aria-hidden="true"></i>
        <span class="nav-link-text">Payment Settings</span>
      </a>
    </li>
    <!-- <li class="nav-item ">
      <a class="nav-link " href="Support">
        <i class="fas fa-envelope" aria-hidden="true"></i>
        <span class="nav-link-text">Support</span>
      </a>
    </li> -->
    <li class="nav-item ">
      <a class="nav-link " href="logout">
        <i class="fa fa-sign-out" aria-hidden="true"></i>
        <span class="nav-link-text">Log out</span>
      </a>
    </li>


  </ul>

</ul>