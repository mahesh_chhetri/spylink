<header class="header_area">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h" href="">
						<img src="dist/img/logo-light.png" alt=""
							style="max-width:200px"></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
						aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav justify-content-center">
							<li class="nav-item active"><a class="nav-link" href="/">Home</a></li>
							<li class="nav-item"><a class="nav-link" href="rates.html">PAYOUT RATE</a></li>
							<li class="nav-item"><a class="nav-link" href="protect">Protect Links</a></li>

							<!-- <li class="nav-item submenu dropdown">
								<a href="dmca.php" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
									aria-haspopup="true" aria-expanded="false">DMCA</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a class="nav-link" href="dmca.php"></a></li>

								</ul>
							</li> -->
							<li class="nav-item"><a class="nav-link" href="dmca">DMCA</a></li>

							<?php 
							if(!$_SESSION['user_id']){
								echo '<li class="nav-item"><a class="nav-link" href="login">LOGIN</a></li>';
							}else{
								echo '<li class="nav-item"><a class="nav-link" href="dashboard">Dashboard</a></li>';
							}
							?>
						</ul>
						<ul class="nav navbar-nav navbar-right">
						<?php 
							if(!$_SESSION['user_id']){
								echo '<li class="nav-item"><a href="signup"
								class="primary_btn text-uppercase">SIGN UP</a></li>';
							}
							?>
							
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</header>